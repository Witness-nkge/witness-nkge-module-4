import 'package:flutter/material.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter_app/homepage.dart';


class Splash extends StatelessWidget {
  const Splash({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: AnimatedSplashScreen(
          splash: 'assets/splash.png',
          nextScreen: const HomePage(),
          splashTransition: SplashTransition.slideTransition,
          duration: 5000,
        )
    );
  }
}